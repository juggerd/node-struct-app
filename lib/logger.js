'use strict';

const moment = require('moment');
const winston = require('winston');

module.exports = new (winston.Logger)({
    exitOnError: false,
    transports: [
	new (winston.transports.File)({
	    filename: __basedir +'/log/general.log',
	    timestamp: function() {
		return getCurrentTime();
	    },
	    handleExceptions: true,
	    humanReadableUnhandledException: true,
	    maxsize: 1024000,
	    maxFiles: 7,
	    json: false
	}),
	new (winston.transports.Console)({
	    handleExceptions: true,
	    humanReadableUnhandledException: true,
	    timestamp: function() {
		return getCurrentTime();
	    },
	    colorize: true,
	    json: false
	}),
    ]
});

let getCurrentTime = function() {
    return new moment().format('YYYY-MM-DD hh:mm:ss-SSS');
};