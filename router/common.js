'use strict';

const express = require('express');
const co = require('co');
const _ = require('lodash');
const moment = require('moment');

let router = express.Router();

let config = require(__basedir +'/config');
let logger = require(__basedir +'/lib/logger');
let fn = require(__basedir +'/lib/fn');

router.get('/', function(req, res) {
    res.status(404).json({ 
	message: 'Page not found' 
    });
});

module.exports = router;