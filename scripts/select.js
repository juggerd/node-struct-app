'use strict';

const path = require('path');
const co = require('co');

global.__basedir = path.resolve(__dirname +'/../');

let config = require(__basedir +'/config');
let fn = require(__basedir + '/lib/fn');
let logger = require(__basedir + '/lib/logger');

co(function* (){
    let conn = yield fn.connect(config.mysql);
    let query = 'SELECT TABLE_SCHEMA base \
                FROM INFORMATION_SCHEMA.TABLES \
                WHERE TABLE_SCHEMA LIKE ? \
                GROUP BY TABLE_SCHEMA';
    let bases = fn.query(conn, query, ['test%']);
    fn.disconnect(conn);
    return bases;
}).then(function(data){
    console.log(data);
}).catch(function(err){
    logger.error(err);
});