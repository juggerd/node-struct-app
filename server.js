'use strict';

global.__basedir = __dirname;

const http = require('http');
let logger = require(__basedir + '/lib/logger');
let config = require(__basedir + '/config');
let app = require(__basedir + '/app');

process.on('uncaughtException', function(err) {
    logger.error('uncaughtException', {
        date: err.date,
        message: err.message,
        stack: err.stack
    });
    //process.exit(1);
});

process.on('ReferenceError', function(err) {
    logger.error('ReferenceError', {
        date: err.date,
        message: err.message,
        stack: err.stack
    });
});

http.createServer(app).listen(config.http.port, config.http.host, function() {
    logger.info('Server Started: '+ config.http.host + ':' + config.http.port);
});
