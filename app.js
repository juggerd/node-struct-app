'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const basicAuth = require('basic-auth');

let app = express();
let config = require(__basedir + '/config');

app.disable('x-powered-by');
app.use(function(req, res, next) {
    let user = basicAuth(req);
    if (!user || 
            user.name !== config.http.auth.login || 
            user.pass !== config.http.auth.passw
        ) {
            res.statusCode = 401;
            res.setHeader('WWW-Authenticate', 'Basic realm="MyRealmName"');
            res.end('Unauthorized');
        } else {
            next();
        }
    });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ 
    extended: true 
}));
app.use('/', require(__basedir + '/router/common'));
app.use('/api', require(__basedir + '/router/api'));
app.use('*', function(req, res) {
    res.status(404).json({ 
    message: 'Page not found' 
    });
});

module.exports = app;